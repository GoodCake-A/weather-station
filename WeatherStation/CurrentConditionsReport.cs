﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherStation.EventsArguments;

namespace WeatherStation
{
    public static class CurrentConditionsReport
    {
        public static void UpdateWeatherReport(object sender, WeatherData data)
        {
            Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine("Current weather: " + data.ToString());
            Console.WriteLine("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
    }
}
