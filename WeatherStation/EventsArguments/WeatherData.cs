﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WeatherStation.EventsArguments
{
    [Serializable]
    public class WeatherData : EventArgs
    {
        public WeatherData(WeatherData source) : this(source.Temperature, source.Humidity, source.Pressure)
        {
        }

        public WeatherData(double temperature, double humidity, double pressure)
        {
            this.Temperature = temperature;
            this.Humidity = humidity;
            this.Pressure = pressure;
        }

        public double Temperature { get; set; }

        public double Humidity { get; set; }

        public double Pressure { get; set; }

        public override string ToString()
        {
            return $"Tepmerature {this.Temperature.ToString()} °C, Pressure {this.Pressure} atm, Humidity {this.Humidity}%";
        }
    }
}
