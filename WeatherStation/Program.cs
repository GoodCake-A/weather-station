﻿using System;
using WeatherStation.EventsArguments;

namespace WeatherStation
{
    class Program
    {
        static void Main(string[] args)
        {
            var culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

            var weatherStation = new WeatherStation();
            var statisticReport = new StatisticReport("statistic.bin");

            weatherStation.UpdateData += statisticReport.AddRecord;
            weatherStation.UpdateData += CurrentConditionsReport.UpdateWeatherReport;

            char controlCharacter;
            do
            {
                Console.WriteLine("");
                Console.WriteLine("Select operation");
                Console.WriteLine("1. Update weather.");
                Console.WriteLine("2. Get average weather");
                Console.WriteLine("q. Exit");
                Console.Write("Enter character:");
                controlCharacter = Convert.ToChar(Console.ReadLine());

                switch (controlCharacter)
                {
                    case '1':
                        Console.Write("Temperature °C:");
                        double temperature = Convert.ToDouble(Console.ReadLine(), culture);
                        Console.Write("Humidity %:");
                        double humidity = Convert.ToDouble(Console.ReadLine(), culture);
                        Console.Write("Pressure atm:");
                        double pressure = Convert.ToDouble(Console.ReadLine(), culture);

                        weatherStation.ReceiveData(new WeatherData(temperature, humidity, pressure));
                        break;
                    case '2':
                        Console.WriteLine("");
                        Console.Write("Average weather: " + statisticReport.GetStatistic().ToString());
                        break;
                }
            } while (char.ToLowerInvariant(controlCharacter) != 'q');

            statisticReport.SaveAddedRecords();
        }
    }
}
