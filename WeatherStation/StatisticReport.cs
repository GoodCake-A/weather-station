﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;
using WeatherStation.EventsArguments;

namespace WeatherStation
{
    public class StatisticReport
    {
        private readonly FileStream weatherHistoryStorage;

        private List<(DateTime time, WeatherData weather)> history;

        private List<(DateTime time, WeatherData weather)> addedRecords;

        public StatisticReport(string storagePath)
        {
            weatherHistoryStorage = new FileStream(storagePath ?? throw new ArgumentNullException(nameof(storagePath)), FileMode.OpenOrCreate, FileAccess.ReadWrite);
            addedRecords = new List<(DateTime time, WeatherData weather)>();
        }

        ~StatisticReport()
        {
            weatherHistoryStorage.Close();
        }

        public void SaveAddedRecords()
        {

            if (addedRecords.Count > 0)
            {
                weatherHistoryStorage.Seek(0, SeekOrigin.End);
                var binaryFormatter = new BinaryFormatter();

                foreach (var element in addedRecords)
                {
                    binaryFormatter.Serialize(weatherHistoryStorage, element.time);
                    binaryFormatter.Serialize(weatherHistoryStorage, element.weather);
                }
            }

            history.AddRange(addedRecords);
            addedRecords.Clear();
        }

        public void AddRecord(object sender, WeatherData argument)
        {
            addedRecords.Add((DateTime.Now, new WeatherData(argument)));
        }

        public WeatherData GetStatistic()
        {
            if (history is null)
            {
                history = new List<(DateTime time, WeatherData weather)>();
                var binaryFormatter = new BinaryFormatter();

                weatherHistoryStorage.Seek(0, SeekOrigin.Begin);
                while (weatherHistoryStorage.Length != weatherHistoryStorage.Position)
                {
                    var time = (DateTime)binaryFormatter.Deserialize(weatherHistoryStorage);
                    var weather = (WeatherData)binaryFormatter.Deserialize(weatherHistoryStorage);

                    history.Add((time, weather));
                }
            }


            var averageWeather = new WeatherData(0, 0, 0);

            foreach (var element in history.Concat(addedRecords))
            {
                averageWeather.Humidity += element.weather.Humidity;
                averageWeather.Pressure += element.weather.Pressure;
                averageWeather.Temperature += element.weather.Temperature;
            }

            averageWeather.Humidity /= history.Count + addedRecords.Count;
            averageWeather.Temperature /= history.Count + addedRecords.Count;
            averageWeather.Pressure /= history.Count + addedRecords.Count;

            return averageWeather;
        }
    }
}
