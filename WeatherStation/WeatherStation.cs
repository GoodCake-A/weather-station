﻿using System;
using System.Collections.Generic;
using System.Text;
using WeatherStation.EventsArguments;

namespace WeatherStation
{
    public class WeatherStation
    {
        public WeatherStation()
        {
        }

        public event Action<object, WeatherData> UpdateData;

        public void ReceiveData(WeatherData data)
        {
            this.UpdateData?.Invoke(this, data);
        }
    }
}
